## 项目信息

- 项目名称：Investigation about Job Workflow

- 方案描述：In some scenarios, a workflow，which consists of some steps and each step stands for 

  one or more jobs, is a good solution. Your task is to take an investigation of existing design and 

  implementation such as Argo, output a report and make a design for Volcano.

- 时间规划：

  07.01-07.15   Do a survey on the workflow in kubernetes, and try to understand what each step 

  stands for . 

  07.16-07.30   Take an investigation of existing design and implementation such as Argo. 

  08.01-08.15   Do a survey on volcano to understand the job workflow in it，then make a 

  comparison with kubernetes. 

  08.16-09.16   Output a report and make a design for Volcano. 

  09.16-09.30   Discuss with the mentor and other students, to make up for any design 

  shortcomings. 


## 项目总结

- 项目产出：

  - Argo Workflow现有的设计思想

    Argo Workflow 专注于 Kubernetes Native Workflow 设计，拥有声明式工作流机制，能够 

    通过 CRD 的模式完全兼容 Kubernetes 集群，每个任务通过 Pod 的形式运行，通过 steps 

    可以很方便的定义按顺序执行的线性任务，如果任务依赖不是线性的而是多层树依赖， 

    则可以通 DAG 有向无环图来定义，每个任务需要明确定义所依赖的其他任务名称。

  - Volcano现有job workflow的设计总结

    目前volcano 就是主要利用argo里面的resource template来创建 Volcano Job，支持的多task的job，有一些线性依赖就选用step工作流，多任务的前后依赖关系则用dag工作流。

  - Argo Workflow的现有的功能调研

    - 测试使用Argo的Parameters参数/Steps步骤/DAG/条件/循环/递归/Timeout等workflow模版
    - 思考不同Workflow不同模版对应的典型场景

  - 追踪最新版本的Argo的典型新特性

    - HTTP Template
    - Inline Template
    - Conditional Based Retry Strategy

  - Volcano taskflow可以借鉴的部分

    -  Argo中的Workflow中允许使用变量
    -  argo的制品库Artifact
    -  HTTP Template and Agent
    -  Retrying Failed or Errored Steps和调度的融合
    -  Timeouts与调度的融合
    -  循环、条件、递归结构的支持

  - Volcano中相关功能点的设计。基于某些HPC场景下，一个作业被分为若干任务。若干个任务之间存在依赖关系，依赖关系使用DAG构建workflow。**使用DAG、支配树、最近公共祖先问题等概念，创新地构建设计了并行调度框架，给出了具体的调度策略和实现方法。**

- 方案进度：

  07.01-07.15   完成

  07.16-07.30   完成

  08.01-08.15   完成 

  08.16-09.30   完成. 

- 遇到的问题及解决方案：

  - Argo的官方文档与源代码都是英文，在理解的过程中提升了阅读英文文档的能力
  - Argo workflow的设计思路与架构的调研入门比较困难，通过与其他工作流引擎的对比来进行理解和学习
  - Workflow所使用的场景和运行状态比较抽象，通过GCS的界面服务和Argo workflow的CLI客户端来进行理解
  - 关于Argo Workflow与Volcano的taskflow如何进行融合， 通过了解Volcano支持的调度场景中可能会产生的依赖关系，分析可以从Argo Workflow可以借鉴的点。
  - **设计DAG上的并行调度框架一开始没有思路，通过调研相关同类开源项目taskflow以及高级操作系统课程中DAG上的拓扑调度，综合调研结果，创新地设计调度算法框架。**

- 项目完成质量：

  1. Argo Workflow的设计以及Volcano现有workflow的使用都进行了全面的调研，官方给出的例子进行了端到端的测试，深入理解了Argo的使用场景以及Volcano调度过程中taskflow 多个任务之间存在的依赖关系，最终给出Volcano可以从Argo Workflow借鉴的设计。
  2. 代码中具体实现的形式可以在社区进一步沟通与讨论。

- 与导师沟通及反馈情况：

  和导师有良好的交流，导师会定期巡检，导师很有耐心，有问题都会及时回答。

### 【注：具体设计方案见附件workflow_report.md, 与Volcano结合的具体方案详见task-DAG-plugin文件夹】







