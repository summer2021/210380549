## Argo workflow产生的背景——Kubernetes Job的问题

​		Kubernetes平台主要运行一些持续运行的应用，即daemon服务，其中最擅长的就是无状态服务托管，比如Web服务，滚动升级rollout和水平扩展scale out都非常方便。

​		而针对基于事件触发的非持续运行的任务，Kubernetes原生能力可以通过Job实现，不过，Job仅解决了单一任务的执行，目前Kubernetes原生还没有提供多任务的编排能力，无法解决多任务的依赖以及数据交互问题。

​		比如启动一个测试任务，首先需要从仓库拉取最新的代码，然后执行编译，最后跑批单元测试。这些小的子任务是串行的，必须在前一个任务完成后，才能继续下一个任务。

​		如果使用Job，不太优雅的做法是**每个任务都轮询上一个任务的状态直到结束**。或者通过initContainers实现，因为initContainer是按顺序执行的，可以把前两个任务放initContainer，最后单元测试放到主Job模板中，这样Job启动时前面的initContainer任务保证是成功执行完毕。

​		不过**initContainer只能解决非常简单的按顺序执行的串行多任务问题**，无法解决一些复杂的非线性任务编排，这些任务的依赖往往形成一个复杂的DAG(有向图)，比如:

![workflow task](https://int32bit.sh/img/posts/Kubernetes%E6%89%A9%E5%B1%95%E7%A5%9E%E5%99%A8Argo%E5%AE%9E%E8%B7%B5/workflow_task.png)

​		这种问题通过Kubernetes的原生能力目前还不能很好的解决。显然如果单纯使用Kubernetes Job很难完美实现，除非在容器中封装一个很复杂的逻辑，实现一个复杂的编排engine，这就不是Job的问题了。

## 什么是workflow？	

​		workflow定义了一个工作的流程模版，让整个流程中的各个环节更加有效、高效的组织起来。下面展示了几个例子。

- 学院财务报销流程：需要负责人、导师、实验室主任、院领导、校领导、财务部的签字最终才能审批通过。但是签字的顺序是有依赖的，类似院领导必须看到主任的签字才签字，校领导必须看到院领导，财务部必须看到所有领导的签字。基于一个DAG的依赖workflow，整个过程就会变得高效，因为很明显我们不会先去找财务部、校领导签字。
- 机器学习落地场景：机器学习的过程中有很多的环节和模块，这里可以看到，除了我们熟知的模型训练环节之外还包括数据收集、预处理、资源管理、特性提取、数据验证、模型的管理、模型发布、监控等环节[[1]](https://support.huaweicloud.com/bestpractice-cce/cce_bestpractice_0075.html)。AI算法工程师来讲，他要做模型训练，就不得不搭建一套AI计算平台，这个过程耗时费力，而且需要很多的知识积累。
- 模拟相互依赖关系的数据处理任务：音频视频转码/机器学习数据流/大数据分析。
- 基因计算：在基因计算场景下使用Argo Workflow 在Kubernetes集群上运行数据处理分析业务，能够支持超过 5000 步以上的大型工作流[[2]](https://developer.aliyun.com/article/754221)。

## 什么是Argo workflow？

​		Argo workflow专门设计解决Kubernetes工作流任务编排问题，基于kubernetes 的调度能力实现了工作流的控制和任务的运行，同时提供了一个 UI 来方便我们查看任务的进程和详情等等。Argo能够帮助用户专注并行作业编排。kubeflow的底层工作流引擎也是Argo workflow。	

- 定义workflow，其中每一个step都是一个container。
- 将多步骤工作流建模为任务序列，或者使用DAG捕获任务之间的依赖关系。
- 使用Kubernetes上的Argo Workflow，可以在短时间内轻松运行用于计算机学习或数据处理的计算密集型作业。
- 无需配置复杂的软件开发产品即可在Kubernetes上本地运行CI/CD管道。

​        Argo 项目作为一组 Kubernetes 原生工具集合，用于运行和管理 Kubernetes 上的作业和应用程序。Argo 提供了一种在 Kubernetes 上创建工作和应用程序的三种计算模式 – 服务模式、工作流模式和基于事件的模式 – 的简单组合方式。所有的 Argo 工具都实现为控制器和自定义资源[[3]](https://www.infoq.cn/article/ffzpvrktbykg53x03iah)。

​		Argo是[Cloud Native Computing Foundation（CNCF）](https://cncf.io/)托管的项目。

## Argo workflow的设计思想

​		Argo Workflow 专注于 Kubernetes Native Workflow 设计，拥有声明式工作流机制，能够通过 CRD 的模式完全兼容 Kubernetes 集群，**每个任务通过 Pod 的形式运行**，通过`steps`可以很方便的定义按顺序执行的线性任务，不过如果任务依赖不是线性的而是多层树依赖，则可以通过`dag`进行定义，`dag`即前面介绍的DAG有向无环图，每个任务需要明确定义所依赖的其他任务名称。

​		Workflow 中每个Template由一个可选的输入部分、一个可选的输出部分和一个容器调用或一个步骤列表组成，其中每个步骤调用另一个Template。Template在argo中代表可运行的节点，一共有六种。分别是 `Container`, `Script`, `Resource`, `Suspend`, `Steps`, `DAG`;

![img](https://static001.infoq.cn/resource/image/c6/4b/c69a40c41d9fcd42651bb4af4da24d4b.png)

​		上图就是一个典型的 DAG 结构，Argo Workflow 可以根据用户提交的编排模板，很容易的构建出一个有相互依赖关系的工作流。Argo Workflow 就可以处理这些依赖关系，并且按照用户设定的顺序依次运行。

​		除了正向依赖关系，Workflow还支持分支、循环、递归等；任务之间除了定义依赖关系，还可以通过input、output实现数据交互，即一个task的output可以作为另一个task的input。除了通过input和output实现数据交互，对于数据比较大的，比如二进制文件，则可以通过Artifacts制品进行共享，这些制品可以是提前准备好的，比如已经存储在git仓库或者s3中，也可以通过任务生成制品供其他任务读取。

## Argo Workflow的设计架构

![img](http://dockone.io/uploads/article/20210113/bbeb281871690be1a693f64d1bb65f1a.png)

​		Argo拥有三大组件：ui/controller/argoexec 

- ui在生产环境用的很少，和团队的前端技术栈有关系

- Argoexec：用于拉起pod的init和wait两个container。（用户定义的内容会在 Pod 中以 Main Container 体现。此外，还有两个 Sidecar 来辅助运行。在 Argo 中，Sidecar 的镜像都是 argoexec。Argo 通过这个 executor 来完成一些流程控制）

> ​		这涉及到实现不同step的参数传递。在创建pod的时候，用gotpl注入参数即可。难度稍高的是文件注入。文件注入argo用的是动态复制文件的方式。算法一般会有输入，也会有输出。argo采用的方式，是在一个pod插入两个container，也就是sidecar 容器，执行的镜像都是argoexec。argoexec的功能实现代码实现在 argoproj/argo/cmd/argoexec/main.go:16
>
> ​		由于有两个sidecar container，处理输入输出的文件变得比较简单，因为Volume是pod内可见的，只需要mount的路径对就行。这样用户的container尽可能保持完整。所以用户空间里面一个pod会有三个容器。


- workflow-controller：监听CRD。

![架构](https://img-blog.csdnimg.cn/20200212170548994.jpeg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L29xcVl1YW4xMjM0NTY3ODkw,size_16,color_FFFFFF,t_70)

​		如图所示，两个Informer，分别监听不同的资源。其中pod与wf(workflow)的交互，主要依赖于argoexec拉起的两个container。

​		**Argo Workflow 每一个具体的任务都是通过 Pod 来执行**，同时有一个 sidecar 容器来监听主任务的进行，sidecar 能够通过 service account 监听 APIServer 来获取到主容器的动态与信息，实现 Kubernetes RBAC 的支持与权限收敛。

​		Argo Workflow 在 DAG 解析过程中，每一步都会根据 Workflow label 来扫描所有的 Pod 状态，以此来决定是否需要进行下一步的动作。并行扫描的功能已经在 Argo Workflow v2.4 版本发布。

​		在实际生产中，Argo Workflow 执行的步数越多，占用的空间越多。所有的执行步骤均记录在 CRD Status 字段里面。当任务数量超过 1000 步的时候，就会出现单个对象过大，无法存储进入 ETCD，或者会因为流量过大，拖垮 APIServer。此处argo的状态压缩技术，能够将 Status 进行字符串压缩。压缩后的 Status 字段大小仅为原来大小的 20 分之一，实现了 5000 步以上的大型工作流运行。

## Argo 的落地实践

- 基因测序领域

  基因测序流程包含测序过程所需工具的执行先后信息以及数据输入输出等定义。流程由至少一个工具组成。流程中的各个工具由其前后顺序关系形成数据流，前序工具为后序工具提供输入。基因容器提供了多套典型的测序流程，基于此流程，可以快速完成测序任务。

- KubeFlow的Pipeline子项目

  全面依赖Argo作为底层实现，并增强持久层来补充流程管理能力，同时通过Python-SDK来简化流程的编写。

## Volcano job 的DAG调度策略

​		Volcano作为基于Kubernetes的云原生批量计算引擎，在它的某些场景下，同样需要增加task级别的DAG调度策略，来提供自定义任务顺序启动的特性。例如以下场景：

- MPI Job。worker必须早于master开始执行。如果master开始执行，但是worker却还没有，这时候master会重启。这将增加一些不必要的资源浪费。在这种情况下,mpi worker必须保证是running的状态才会为 MPI 的master创建pod。

- 假设有两个任务，task2需要使用task1的计算结果。在这种情况下，task1需要处于complete状态，才会为task2创建pod。

- ......

  因此，完全可以考虑借鉴Argo Workflow的设计思想为volcano设计 task级别的DAG调度策略。

## 参考

[1] https://support.huaweicloud.com/bestpractice-cce/cce_bestpractice_0075.html

[2] https://developer.aliyun.com/article/754221

[3] https://www.infoq.cn/article/ffzpvrktbykg53x03iah

[4] https://argoproj.github.io/workflows/

[5] http://dockone.io/article/1464917

[6] https://int32bit.sh/2020/08/12/Kubernetes%E6%89%A9%E5%B1%95%E7%A5%9E%E5%99%A8Argo%E5%AE%9E%E8%B7%B5/











